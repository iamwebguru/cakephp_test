 <section class="content-header">
      <h1>
        Appointments
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Doctor</a></li>
        <li class="active">Appointments</li>
      </ol>
    </section>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
    <div class="box">
           <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Appointment Name</th>
                  <th>Patient Name</th>
                  <th>Appointment Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1;
                  foreach($tasks as $key=>$value){ ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $value['appointment_name']; ?></td>
                  <td><?php echo $value['User']['full_name'];?></td>
                  <td><?php echo date('d-M-Y',strtotime($value['appointment_date'])); ?></td>
                  <td><a href="<?php echo BASE_URL; ?>/doctor/edit/<?php echo $value['id']; ?>">Edit</a>
                    <a href="<?php echo BASE_URL; ?>/doctor/delete/<?php echo $value['id']; ?>">Delete</a></td>
                </tr>
                <?php } ?>
               
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Appointment Name</th>
                  <th>Patient Name</th>
                  <th>Appointment Date</th>
                  <th>Action</th
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
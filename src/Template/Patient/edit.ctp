<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Apointments</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php echo $this->Form->create('Appointments'); ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Appointment Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Appointment Name" name="Appointments[appointment_name]" required value="<?php echo $appointments['appointment_name']?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Appointment Date</label>
                  <input type="text" class="form-control"  placeholder="Appointment Date" id="datepicker" name="Appointments[appointment_date]" required value="<?php echo date('Y-m-d',strtotime($appointments['appointment_date']));?>">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Doctor</label>
                  <select class="form-control" name="Appointments[doctor_id]" required>
                   <option value="">Please Select</option>
                   <?php foreach($users as $user){ ?>
                   <option value="<?php echo $user['id'] ?>" <?php if($appointments['doctor_id']==$user['id']){ echo "selected=selected"; } ?>><?php echo $user['full_name']; ?></option>
                   <?php } ?>
                  </select>
                </div>
                
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
     <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="<?php echo $this->request->webroot ?>doctor">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
         </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Appointments</span>
            <span class="pull-right-container">
              <!-- <span class="label label-primary pull-right">4</span> -->
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if($this->request->params['controller']=='Doctor' && $this->request->params['action']=='index'){ ?>
            <li><a href="<?php echo BASE_URL ;?>/doctor/appointments"><i class="fa fa-circle-o"></i> appointments</a></li>
            <?php } ?>

            <?php if($this->request->params['controller']=='Patient' && $this->request->params['action']=='index'){ ?>
                   <li><a href="<?php echo BASE_URL ;?>/patient/appointments"><i class="fa fa-circle-o"></i> View appointments</a></li>
                   <li><a href="<?php echo BASE_URL ;?>/patient/addAppointments"><i class="fa fa-circle-o"></i> Add appointments</a></li>
            <?php } ?>
            
          </ul>
        </li>
        </ul>

    </section>
    <!-- /.sidebar -->
  </aside>
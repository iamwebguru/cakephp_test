<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class RegisterController extends AppController
{
public function index()
    {
       $this->viewBuilder()->layout('admin');
       $this->loadModel('Users');
       if($this->request->is('post')){
       	        $stu_data=$this->Users->find('all',array('conditions'=>array('Users.email'=>$this->request->data['Users']['email'])))->first();
		       	if(!empty($stu_data['email']))
		       	{
		       		$this->Flash->warning('Email address already exists',['key'=>'positive']);
		            $this->redirect(array('controller'=>'users','action'=>'register'));
		       	}else{
		       		if($this->request->data['Users']['password']==$this->request->data['Users']['con_password'])
		       		{
                              $password=Security::hash($this->request->data['Users']['password'],null,true);
						       $articlesTable = TableRegistry::get('Users');
						        $article = $articlesTable->newEntity();
						        $article->full_name=$this->request->data['Users']['full_name'];
						        $article->email=$this->request->data['Users']['email'];
						        $article->password=$password;
						        $article->role=$this->request->data['Users']['role'];
						        if($articlesTable->save($article))
						        {
                                      $this->redirect(array('controller'=>'users','action'=>'login')); 
						        }
		       		}
		       		else
		       		{
                            $this->Flash->warning('Password does not match, please try again', ['key' => 'positive']);
                            $this->redirect(array('controller'=>'users','action'=>'register'));
		       		}
		       	}
       }
    }
}
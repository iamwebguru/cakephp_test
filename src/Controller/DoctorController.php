<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
class DoctorController extends AppController
{
     public function index()
    {
       $this->viewBuilder()->layout('main');
    }

    public function appointments()
    {
      $this->loadModel('Appointments');
      $this->loadModel('Users');
      $tasks=$this->Appointments->find('all')->toArray();
      //$doctor_id=array();
      foreach($tasks as $key=>$task){
      	$doctor_id=$task['patient_id'];
      	$users= $this->Users->find('all',array('conditions'=>array('Users.id'=>$doctor_id)))->first();
      	$tasks[$key]['User']=$users;
      }
      //pr($tasks);
      $this->set('tasks',$tasks);

    }
}


<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
class PatientController extends AppController
{
   public function index()
    {
     $this->viewBuilder()->layout('main');
    }


    public function appointments()
    {
      $this->viewBuilder()->layout('main');
      $this->loadModel('Appointments');
      $this->loadModel('Users');
      $tasks=$this->Appointments->find('all')->toArray();
      //$doctor_id=array();
      foreach($tasks as $key=>$task){
      	$doctor_id=$task['doctor_id'];
      	$users= $this->Users->find('all',array('conditions'=>array('Users.id'=>$doctor_id)))->first();
      	$tasks[$key]['User']=$users;
      }
      //pr($tasks);
      $this->set('tasks',$tasks);
    } 

     public function addAppointments()
    {
      $this->viewBuilder()->layout('main');
      $this->loadModel('Appointments');
      $this->loadModel('Users');
      $users=$this->Users->find('all',array('conditions'=>array('Users.role'=>'doctor')))->toArray();
      $this->set('users',$users);
      if($this->request->is('post')){
      	$date=date('Y-m-d', strtotime($this->request->data['Appointments']['appointment_date']));
      	//pr($this->request->data); die;
      	         $articlesTable = TableRegistry::get('Appointments');
						        $article = $articlesTable->newEntity();
						      $article->appointment_name=$this->request->data['Appointments']['appointment_name'];
						      $article->appointment_date=$date;
						        $article->doctor_id=$this->request->data['Appointments']['doctor_id'];
						        $article->patient_id=$_SESSION['Auth']['User']['id'];
						        if($articlesTable->save($article))
						        {
                                      $this->redirect(array('controller'=>'patient','action'=>'appointments')); 
						        }
      }
    }

    public function edit($id=null)
    {
    	$this->loadModel('Users');
    	$this->loadModel('Appointments');
     $this->viewBuilder()->layout('main');
      $users=$this->Users->find('all',array('conditions'=>array('Users.role'=>'doctor')))->toArray();
      $this->set('users',$users);
      $appointments=$this->Appointments->find('all',array('conditions'=>array('Appointments.id'=>$id)))->first();
      $this->set('appointments',$appointments);
      if($this->request->is('post')){
      $date=date('Y-m-d', strtotime($this->request->data['Appointments']['appointment_date']));
      	//pr($this->request->data); die;
      	         $articlesTable = TableRegistry::get('Appointments');
						        $article = $articlesTable->newEntity();
						      $article->appointment_name=$this->request->data['Appointments']['appointment_name'];
						      $article->appointment_date=$date;
						        $article->doctor_id=$this->request->data['Appointments']['doctor_id'];
						        $article->id=$id;
						        if($articlesTable->save($article))
						        {
                                      $this->redirect(array('controller'=>'patient','action'=>'appointments')); 
						        }

	 }
    }
} 


<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class UsersController extends AppController
{

	// public function beforeFilter(){
 //    parent::beforeFilter();
 //    $this->Auth->allow('register');
 //     }
      public function login()
    {
       $this->viewBuilder()->layout('admin');
		         $this->loadModel('Users');
		         if($this->request->is('post'))
		         {
					        $conditions=array(
					            'Users.email'=>$this->request->data['Users']['email'],
					         'Users.password'=>Security::hash($this->request->data['Users']['password'],null,true));
					        $students=$this->Users->find('all',array('conditions'=>$conditions))->first();
					        if(!empty($students))
					        {
						            $student_data=$students->toArray();
						            if ($student_data) 
						            {
						                $this->Auth->setUser($student_data);
						                if($students['role']=='doctor')
						                {
						                   $this->redirect(array('controller'=>'doctor','action'=>'index'));
						                }else{
						                   $this->redirect(array('controller'=>'patient','action'=>'index'));
						                }
						                 
						            }else{

						             $this->Flash->warning('Invalid username or password',['key'=>'positive']);
						             $this->redirect(array('controller'=>'login','action'=>'index'));
						            }
					       }
		       }
    }

public function register()
    {
       $this->viewBuilder()->layout('admin');
       $this->loadModel('Users');
       if($this->request->is('post')){
       	        $stu_data=$this->Users->find('all',array('conditions'=>array('Users.email'=>$this->request->data['Users']['email'])))->first();
		       	if(!empty($stu_data['email']))
		       	{
		       		$this->Flash->warning('Email address already exists',['key'=>'positive']);
		            $this->redirect(array('controller'=>'users','action'=>'register'));
		       	}else{
		       		if($this->request->data['Users']['password']==$this->request->data['Users']['con_password'])
		       		{
                              $password=Security::hash($this->request->data['Users']['password'],null,true);
						       $articlesTable = TableRegistry::get('Users');
						        $article = $articlesTable->newEntity();
						        $article->full_name=$this->request->data['Users']['full_name'];
						        $article->email=$this->request->data['Users']['email'];
						        $article->password=$password;
						        $article->role=$this->request->data['Users']['role'];
						        if($articlesTable->save($article))
						        {
                                      $this->redirect(array('controller'=>'users','action'=>'login')); 
						        }
		       		}
		       		else
		       		{
                            $this->Flash->warning('Password does not match, please try again', ['key' => 'positive']);
                            $this->redirect(array('controller'=>'users','action'=>'register'));
		       		}
		       	}
       }
    }
    


    public function logout(){
      $this->request->session()->delete('Auth.User');
      $this->redirect(array('controller'=>'users','action'=>'login'));
    }
}

